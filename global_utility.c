/*
** global_utility.c for irc in /home/barbis_j/Documents/Projets/my_irc
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Wed Apr  8 15:58:47 2015 Joseph Barbisan
** Last update Thu Apr  9 14:37:20 2015 Joseph Barbisan
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>

int		xselect(int fd_max, fd_set *fd_read, fd_set *fd_write)
{
  if (select(fd_max + 1, fd_read, fd_write, NULL, NULL) == -1)
    {
      perror("select");
      return (-1);
    }
  return (0);
}
