/*
** server.h for ftp in /home/barbis_j/Documents/Projets/PSU_2014_myftp/server
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Wed Mar 11 17:12:45 2015 Joseph Barbisan
** Last update Sun Apr 12 20:07:35 2015 Joseph Barbisan
*/

#ifndef SERVER_H_
# define SERVER_H_

# include <sys/types.h>
# include <sys/socket.h>
# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <netinet/in.h>
# include <netinet/ip.h>
# include <arpa/inet.h>
# include <string.h>

# define FD_FREE 0
# define FD_CLIENT 1
# define FD_SERVER 2

# define MAX_FD 255

typedef void(*fct)();

typedef struct		s_circ_buffer
{
  char			buffer[4096];
  int			fd;
  int			flag;
  int			empty;
  int			fd_max;
}			t_circ_buffer;

typedef struct	s_env
{
  char		fd_type[MAX_FD];
  fct		fct_read[MAX_FD];
  fct		fct_write[MAX_FD];
  char		nicknames[MAX_FD][20];
  char		channel[MAX_FD][20];
  int		port;
  int		fd_max;
  int		fd_count;
  t_circ_buffer	circular_buf[20];
}		t_env;

int		parser(char *buffer, t_env *e, int fd);
int		server_channel(t_env *e, char *str, int fd);
int		server_subget(int fd, long long size, int filefd);
int		server_loop(t_env *e, struct sockaddr_in *server);
int		server_nickname(t_env *e, char *str, int fd);
int		server_users(t_env *e, char *str, int fd);
int		add_server(t_env *e, char **av, struct sockaddr_in *);
void		client_read(t_env *e, int fd, struct sockaddr_in *);
void		server_read(t_env *e, int fd, struct sockaddr_in *);
int		server_accept(int socketfd, struct sockaddr_in *, t_env *e);
void		client_write(t_env *e, int fd, struct sockaddr *server);
void		server_write(t_env *e, int fd, struct sockaddr *server);
int		xselect(int fd_max, fd_set *fd_read, fd_set *fd_write);
void		client_subread(t_env *e, int fd, char *buf);

#endif /* !SERVER_H_ */
