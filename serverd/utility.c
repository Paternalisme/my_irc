/*
** utility.c for irc in /home/barbis_j/Documents/Projets/my_irc
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Wed Apr  8 17:07:26 2015 Joseph Barbisan
** Last update Sun Apr 12 21:13:08 2015 Joseph Cosnier
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include "server.h"

void		server_subloop(t_env *e,
			       struct sockaddr_in *server,
			       fd_set *fd_read,
			       fd_set *fd_write)
{
  int		i;

  i = 0;
  while (i < MAX_FD)
    {
      if (FD_ISSET(i, fd_read))
	e->fct_read[i](e, i, server);
      if (FD_ISSET(i, fd_write))
	e->fct_write[i](e, i, server);
      ++i;
    }
  usleep(10);
}

void		server_subloop2(t_env *e,
			       struct sockaddr_in *server,
			       fd_set *fd_read,
			       fd_set *fd_write)
{
  int		i;

  i = 0;
  while (i < MAX_FD)
    {
      if (e->fd_type[i] != FD_FREE)
	{
	  FD_SET(i, fd_read);
	  FD_SET(i, fd_write);
	  e->fd_max = i;
	}
      ++i;
    }
}

int		server_loop(t_env *e, struct sockaddr_in *server)
{
  fd_set	fd_read;
  fd_set	fd_write;
  int		i;

  while (1)
    {
      FD_ZERO(&fd_read);
      FD_ZERO(&fd_write);
      e->fd_max = 0;
      e->fd_count = 0;
      server_subloop2(e, server, &fd_read, &fd_write);
      if (xselect(e->fd_max + 1, &fd_read, &fd_write) == -1)
	return (-1);
      server_subloop(e, server, &fd_read, &fd_write);
    }
  return (1);
}

int		parser(char *buffer, t_env *e, int fd)
{
  static int	(*fptr[3])(t_env *e, char *, int) = {server_nickname,
						     server_users,
						     server_channel};
  static char  	*tab[3] = {"NICK", "USERS", "JOIN"};
  char		*save;
  char		*token;
  char		*token2;
  int		i;

  save = strdup(buffer);
  if ((token = strtok(save, " ")) == NULL)
    token = strdup(buffer);
  save = strdup(token);
  if ((token2 = strtok(save, "\r\n")) == NULL)
    token2 = token;
  i = 0;
  while (i < 3 && strcmp(tab[i], token2) != 0)
    ++i;
  if (i < 3)
    return fptr[i](e, buffer, fd);
  return (0);
}

void		server_write(t_env *e, int fd, struct sockaddr *server)
{
}
