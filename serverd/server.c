/*
** main.c for irc in /home/barbis_j/Documents/Projets/my_irc
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Tue Apr  7 14:09:38 2015 Joseph Barbisan
** Last update Sun Apr 12 20:22:10 2015 Joseph Barbisan
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include "server.h"

void		client_read(t_env *e, int fd, struct sockaddr_in *server)
{
  int		r;
  char		buf[4096];
  int		i;

  r = read(fd, buf, 4095);
  if (r > 0)
    {
      buf[r] = '\0';
      client_subread(e, fd, buf);
    }
  else
    {
      printf("%d: Connection closed\n", fd);
      close(fd);
      e->fd_type[fd] = FD_FREE;
    }
}

void		client_write(t_env *e, int fd, struct sockaddr *server)
{
  int		i;

  i = 0;
  while (i < 20)
    {
      if (e->circular_buf[i].empty != 0 &&
	  (fd == e->circular_buf[i].flag ||
	   (e->circular_buf[i].flag == 0 &&
	    strcmp(e->channel[fd],
		   e->channel[e->circular_buf[i].fd]) == 0)))
	{
	  write(fd, e->nicknames[e->circular_buf[i].fd],
		strlen(e->nicknames[e->circular_buf[i].fd]));
	  write(fd, " : ", 3);
	  write(fd, e->circular_buf[i].buffer,
		strlen(e->circular_buf[i].buffer));
	  --e->circular_buf[i].fd_max;
	  if (e->circular_buf[i].fd_max <= 1 || fd == e->circular_buf[i].flag)
	    {
	      e->circular_buf[i].empty = 0;
	      e->circular_buf[i].buffer[0] = 0;
	    }
	}
      ++i;
    }
}

void		server_read(t_env *e,
			    int fd,
			    struct sockaddr_in *server)
{
  int		acceptfd;
  int		size;
  char		buffer[4096];
  int		i;

  if ((acceptfd = server_accept(fd, server, e)) == -1)
    return;
}

void		init_buffer(t_env *e)
{
  int		i;

  i = 0;
  while (i < 20)
    {
      e->circular_buf[i].empty = 0;
      e->circular_buf[i].fd_max = 0;
      ++i;
    }
  i = 0;
  while (i < MAX_FD)
    {
      e->channel[i][0] = 0;
      ++i;
    }
}

int			main(int ac, char **av)
{
  int			socketfd;
  struct sockaddr_in	server;
  int			acceptfd;
  t_env			e;

  if (ac != 2)
    {
      printf("Usage : ./server port\n");
      return (EXIT_FAILURE);
    }
  init_buffer(&e);
  memset(e.fd_type, FD_FREE, MAX_FD);
  e.port = atoi(av[1]);
  if ((socketfd = add_server(&e, av, &server)) == -1)
    return (EXIT_FAILURE);
  server_loop(&e, &server);
  close(socketfd);
  return (EXIT_SUCCESS);
}
