/*
** server_init.c for irc in /home/barbis_j/Documents/Projets/my_irc/server
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Tue Apr  7 15:45:44 2015 Joseph Barbisan
** Last update Wed Apr  8 16:49:01 2015 Joseph Barbisan
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include "server.h"

int		server_socket()
{
  int		socketfd;

  socketfd = socket(AF_INET, SOCK_STREAM, 0);
  if (socketfd == -1)
    {
      perror("socket");
      return (-1);
    }
  return (socketfd);
}

int		server_bind(struct sockaddr_in *server,
			    int socketfd,
			    char *port)
{
  int		ret;

  server->sin_family = AF_INET;
  server->sin_port = htons(atoi(port));
  server->sin_addr.s_addr = htonl(INADDR_ANY);
  ret = bind(socketfd, (struct sockaddr *)server, sizeof(*server));
  if (ret == -1)
    {
      perror("bind");
      return (-1);
    }
  return (0);
}

int		server_listen(int socketfd)
{
  int		ret;

  ret = listen(socketfd, 4);
  if (ret == -1)
    {
      perror("listen");
      return (-1);
    }
  return (0);
}

int		server_accept(int socketfd, struct sockaddr_in *server, t_env *e)
{
  int		addrlen;
  int		acceptfd;

  addrlen = sizeof(server);
  acceptfd = accept(socketfd, (struct sockaddr *)server,
		    (socklen_t *)&addrlen);
  if (acceptfd == -1)
    {
      perror("accept");
      return (-1);
    }
  e->fd_type[acceptfd] = FD_CLIENT;
  e->fct_read[acceptfd] = client_read;
  e->fct_write[acceptfd] = client_write;
  sprintf(e->nicknames[acceptfd], "%d", acceptfd);
  return(acceptfd);
}

int		add_server(t_env *e, char **av, struct sockaddr_in *server)
{
  int		socketfd;

  if (((socketfd = server_socket()) == -1) ||
      server_bind(server, socketfd, av[1]) == -1 ||
      server_listen(socketfd) == -1)
    return (-1);
  e->fd_type[socketfd] = FD_SERVER;
  e->fct_read[socketfd] = server_read;
  e->fct_write[socketfd] = server_write;
  return (socketfd);
}
