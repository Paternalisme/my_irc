/*
** command.c for irc in /home/barbis_j/Documents/Projets/my_irc/serverd
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Sun Apr 12 18:46:33 2015 Joseph Barbisan
** Last update Sun Apr 12 19:32:47 2015 Joseph Barbisan
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "server.h"

int		server_channel(t_env *e, char *str, int fd)
{
  str[strlen(str) - 1] = 0;
  strcpy(e->channel[fd], &str[5]);
  return (1);
}

int		server_nickname(t_env *e, char *str, int fd)
{
  str[strlen(str) - 1] = 0;
  strcpy(e->nicknames[fd], &str[5]);
  return (1);
}

void		server_subusers(t_env *e, char *users)
{
  int		i;

  i = 0;
  users[0] = 0;
  while (i < MAX_FD)
    {
      if (e->fd_type[i] == FD_CLIENT)
	{
	  strcat(users, e->nicknames[i]);
	  strcat(users, "\n");
	}
      ++i;
    }
}

int		server_users(t_env *e, char *str, int fd)
{
  int		i;
  char		users[5101];

  server_subusers(e, users);
  i = 0;
  while (i < 20)
    {
      if (e->circular_buf[i].empty == 0)
	{
	  strcpy(e->circular_buf[i].buffer, users);
	  e->circular_buf[i].fd = fd;
	  e->circular_buf[i].flag = fd;
	  e->circular_buf[i].empty = 1;
	  e->circular_buf[i].fd_max = e->fd_count;
	  return (1);
	}
      ++i;
    }
  return (0);
}
