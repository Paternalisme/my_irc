/*
** utility2.c for irc in /home/barbis_j/Documents/Projets/my_irc
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Sun Apr 12 19:52:17 2015 Joseph Barbisan
** Last update Sun Apr 12 20:22:19 2015 Joseph Barbisan
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include "server.h"

void		fill_buf(t_env *e, int fd, int i)
{
  e->circular_buf[i].fd = fd;
  e->circular_buf[i].flag = 0;
  e->circular_buf[i].empty = 1;
}

void		client_subread(t_env *e, int fd, char *buf)
{
  int		i;
  int		j;

  i = 0;
  if (parser(buf, e, fd) == 0)
    while (i < 20)
      {
	if (e->circular_buf[i].empty == 0)
	  {
	    strcpy(e->circular_buf[i].buffer, buf);
	    fill_buf(e, fd, i);
	    j = 0;
	    while (j < MAX_FD)
	      {
		if (e->fd_type[j] != FD_FREE &&
		    strcmp(e->channel[j], e->channel[fd]) == 0)
		  ++e->fd_count;
		++j;
	      }
	    e->circular_buf[i].fd_max = e->fd_count;
	    return ;
	  }
	++i;
      }
}
