##
## Makefile for ftp in /home/barbis_j/Documents/Projets/PSU_2014_myftp
## 
## Made by Joseph Barbisan
## Login   <barbis_j@epitech.net>
## 
## Started on  Wed Mar 11 18:08:39 2015 Joseph Barbisan
## Last update Sun Apr 12 21:22:57 2015 Joseph Cosnier
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable -g

NAME1	= server

NAME2	= client

SRCS1	= serverd/server.c \
	  serverd/command.c \
	  serverd/utility.c \
	  serverd/server_init.c \
	  global_utility.c \
	  serverd/utility2.c

SRCS2	= clientd/client.c \
	  clientd/client_command.c \
	  global_utility.c

OBJS1	= $(SRCS1:.c=.o)

OBJS2	= $(SRCS2:.c=.o)


all: $(NAME1) $(NAME2)

$(NAME1): $(OBJS1)
	$(CC) -o $(NAME1) $(OBJS1)

$(NAME2): $(OBJS2)
	$(CC) -o $(NAME2) $(OBJS2)

clean:
	$(RM) $(OBJS1)
	$(RM) $(OBJS2)

fclean: clean
	$(RM) $(NAME1)
	$(RM) $(NAME2)

re: fclean all

.PHONY: all clean fclean re
