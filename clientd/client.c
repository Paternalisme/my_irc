/*
** client.c for irc in /home/barbis_j/Documents/Projets/my_irc/clientd
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Sun Apr 12 18:53:05 2015 Joseph Barbisan
** Last update Sun Apr 12 21:15:39 2015 Joseph Cosnier
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include "client.h"

int		client_socket()
{
  int		socketfd;

  socketfd = socket(AF_INET, SOCK_STREAM, 0);
  if (socketfd == -1)
    {
      perror("socket");
      return (-1);
    }
  return (socketfd);
}

int		client_connect(int socketfd,
			       struct sockaddr_in *client,
			       char *port,
			       char *machine)
{
  int		ret;
  char		*username;
  char		*hostname;

  if ((username = getenv("USER")) == NULL)
    username = "user";
  if ((hostname = getenv("HOST")) == NULL)
    hostname = "hostname";
  client->sin_family = AF_INET;
  client->sin_port = htons(atoi(port));
  client->sin_addr.s_addr = inet_addr(machine);
  ret = connect(socketfd, (struct sockaddr *)client, sizeof(*client));
  if (ret == -1)
    {
      perror("connect");
      return (1);
    }
  dprintf(socketfd, "USER %s %s servername :%s\r\n", username, hostname, username);
  return (0);
}

int			client_parser(int socketfd)
{
  char			buffer[4096];
  int			size;
  char			*token;
  int			i;
  static int		(*fptr[4])(int, char *) = {client_users, client_nick,
   						   client_join, command_error};
  static char		*tab[3] = {"/users", "/nick", "/join"};

  i = 0;
  size = read(0, buffer, 4095);
  buffer[size] = 0;
  if (buffer[0] == '/')
    {
      token = strtok(buffer, " \t\n");
      printf("%s\n", token);
      while (i != 3 && strcmp(tab[i], token) != 0)
	{
	  printf("%s - %s : %d\n", tab[i], token, strcmp(tab[i], token));
	  ++i;
	}
      return (fptr[i])(socketfd, buffer);
    }
  else
    write(socketfd, buffer, size);
  return (0);
}

void		server_parser(int socketfd)
{
  int		size;
  char		buffer[4096];

  size = read(socketfd, buffer, 4095);
  buffer[size] = 0;
  write(1, buffer, size);
}

int			main(int ac, char **av)
{
  int			socketfd;
  struct sockaddr_in	client;
  fd_set		fd_read;

  if (ac != 3)
    {
      printf("Usage : ./client machine port\n");
      return (EXIT_FAILURE);
    }
  if ((socketfd = client_socket()) == -1 ||
      client_connect(socketfd, &client, av[2], av[1]) == -1)
    return (EXIT_FAILURE);
  while (1)
    {
      FD_ZERO(&fd_read);
      FD_SET(0, &fd_read);
      FD_SET(socketfd, &fd_read);
      if (select(socketfd + 1, &fd_read, NULL, NULL, NULL) == -1)
	return (-1);
      if (FD_ISSET(0, &fd_read))
	client_parser(socketfd);
      if (FD_ISSET(socketfd, &fd_read))
	server_parser(socketfd);
    }
  return (EXIT_SUCCESS);
}
