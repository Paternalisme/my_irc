/*
** client_command.c for irc in /home/barbis_j/Documents/Projets/my_irc/clientd
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Sun Apr 12 18:44:04 2015 Joseph Barbisan
** Last update Sun Apr 12 21:03:55 2015 Joseph Cosnier
*/

#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include "client.h"

int		client_users(int fd, char *str)
{
  write(fd, "USERS\r\n", 7);
  return (0);
}

int		client_nick(int fd, char *str)
{
  char		*arg;

  arg = strtok(NULL, " \t\n");
  if (arg == NULL)
    {
      write(1, "Error : New nickname is empty\n", 19);
      return (1);
    }
  else
    {
      printf("New nick : %s\n", arg);
      dprintf(fd, "NICK %s\n", arg);
    }
  return (0);
}

int		client_join(int fd, char *str)
{
  char		*arg;

  arg = strtok(NULL, " \t");
  printf("%s\n", arg);
  if (arg == NULL)
    {
      write(1, "Error : Join takes a channel name as a parameter\n", 49);
      return (1);
    }
  dprintf(fd, "JOIN #%s\r\n", arg);
  return (0);
}

int		command_error(int fd, char *str)
{
  write(1, "Error : Command not recognized\n", 31);
  return (1);
}
