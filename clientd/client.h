/*
** client.h for ftp in /home/barbis_j/Documents/Projets/PSU_2014_myftp/server
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Wed Mar 11 17:53:50 2015 Joseph Barbisan
** Last update Sun Apr 12 21:17:13 2015 Joseph Cosnier
*/

#ifndef CLIENT_H_
# define CLIENT_H_

int		client_login(int socketfd);
int		client_users(int fd, char *str);
int		client_nick(int fd, char *str);
int		client_join(int fd, char *str);
int		command_error(int fd, char *str);
int		xselect(int fd_max, fd_set *fd_read);

#endif
